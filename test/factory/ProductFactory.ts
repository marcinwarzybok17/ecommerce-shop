import { Product, ProductProps } from "../../src/domain/entity/Product";
import * as faker from "faker";
import { allowStaticMethods } from "../../src/utils/allowStaticMethods";
import { EntityFactory } from "./EntityFactory";
import { CurrencyFactory } from "./CurrencyFactory";
import {
  AvaiableCurrencies,
  Currency,
} from "../../src/domain/value-object/Currency";

interface ProductPropsFactory extends Omit<ProductProps, "currency"> {
  currency: AvaiableCurrencies;
}

@allowStaticMethods<EntityFactory<Product, ProductPropsFactory>>()
export class ProductFactory {
  static createOne(params?: Partial<ProductPropsFactory>) {
    const product = new Product({
      currency: CurrencyFactory.createOne(params?.currency),
      description: faker.lorem.paragraph(5),
      image: faker.system.filePath(),
      name: faker.commerce.productName(),
      pricePerUnit: Number(faker.commerce.price(5, 100)),
      ...params,
    });
    return product;
  }
}
