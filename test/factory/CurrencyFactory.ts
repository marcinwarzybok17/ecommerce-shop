import {
  AvaiableCurrencies,
  Currency,
} from "../../src/domain/value-object/Currency";
import { allowStaticMethods } from "../../src/utils/allowStaticMethods";
import * as faker from "faker";
import { ValueObjectFactory } from "./ValueObjectFactory";

@allowStaticMethods<ValueObjectFactory<Currency, AvaiableCurrencies>>()
export class CurrencyFactory {
  static createOne(value?: AvaiableCurrencies): Currency {
    const currency = new Currency(
      value ||
        faker.random.arrayElement([
          AvaiableCurrencies.EUR,
          AvaiableCurrencies.PLN,
        ])
    );

    return currency;
  }
}
