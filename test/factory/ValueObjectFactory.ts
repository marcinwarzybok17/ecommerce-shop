export interface ValueObjectFactory<T, K> {
  createOne(value?: K): T;
}
