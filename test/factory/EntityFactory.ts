export interface EntityFactory<T, K> {
  createOne(entityParams?: Partial<K>): T;
}
