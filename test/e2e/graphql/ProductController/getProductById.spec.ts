import { MainApp } from "../../../../src/MainApp";
import { ProductFactory } from "../../../factory/ProductFactory";
import { ProductRepository } from "../../../../src/infrastructure/repository/ProductRepository";
import "reflect-metadata";
import { ApolloServer } from "apollo-server";
import { ProductMapper } from "../../../../src/domain/mapper/ProductMap";
import { GRAPHQL_ERROR } from "../../../../src/application/graphql/errors/GraphqlError";

describe("getProductById query", () => {
  let main: MainApp;
  let apolloServer: ApolloServer;
  let productRepository: ProductRepository;
  const getProductByIdQuery = `
        query GetProductById($id: Int!) {
            getProductById(productId: $id) {
                id
                name
                description
                pricePerUnit
                currency
                image
            }
        }
    `;

  beforeAll(async () => {
    main = await MainApp.getMainApp();
    await main.start();
    apolloServer = main.graphqlServer.getServerInstance();
    productRepository = new ProductRepository();
  });

  afterAll(async () => {
    await main.stop();
  });

  it("should return product by its id", async () => {
    const product = ProductFactory.createOne();
    const createdProduct = await productRepository.insert(product);
    const result = await apolloServer.executeOperation({
      query: getProductByIdQuery,
      variables: {
        id: createdProduct.getId,
      },
    });
    expect(result.errors).toBeUndefined();
    expect(result.data.getProductById).toEqual(
      ProductMapper.fromDTOToGraphqlResult(
        ProductMapper.fromDomainToDTO(createdProduct)
      )
    );
  });

  it("should throw product not found error when try to get missing product", async () => {
    const result = await apolloServer.executeOperation({
      query: getProductByIdQuery,
      variables: {
        id: -1,
      },
    });

    expect(result.errors[0].extensions.code).toEqual(
      GRAPHQL_ERROR.NOT_FOUND_ERROR
    );
    expect(result.data).toBeNull();
  });
});
