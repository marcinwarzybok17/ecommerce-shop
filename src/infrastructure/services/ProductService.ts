import { DomainError } from "../../domain/common/DomainError";
import { ProductDTO } from "../../domain/dto/ProductDTO";
import { ProductMapper } from "../../domain/mapper/ProductMap";
import { IProductRepository } from "../../infrastructure/repository/ProductRepository";
import {
  InfrastructureError,
  INFRASTRUCTURE_ERROR,
} from "../common/InfrastructureError";

export interface IProductService {
  getAllProductsByPage(page: number): Promise<ProductDTO[]>;
  getProductById(productId: number): Promise<ProductDTO>;
}

export class ProductService implements IProductService {
  constructor(private productRepository: IProductRepository) {}

  async getAllProductsByPage(page: number): Promise<ProductDTO[]> {
    const products = await this.productRepository.getAllProductsByPage(page);

    return products.map((product) => {
      return ProductMapper.fromPersistanceToDTO(product);
    });
  }

  async getProductById(productId: number): Promise<ProductDTO> {
    const productInModel = await this.productRepository.getProductById(
      productId
    );

    if (!productInModel) {
      throw new InfrastructureError(
        INFRASTRUCTURE_ERROR.NOT_FOUND_ERROR,
        "Product not found"
      );
    }
    const product = ProductMapper.fromPersistanceToDomain(productInModel);

    return ProductMapper.fromDomainToDTO(product);
  }
}
