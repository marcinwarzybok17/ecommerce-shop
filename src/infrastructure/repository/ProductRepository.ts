import { Connection, getConnection } from "typeorm";
import { Product } from "../../domain/entity/Product";
import { Product as ProductModel } from "../model/Product";

export interface IProductRepository {
  getAllProductsByPage(page: number): Promise<ProductModel[]>;
  getProductById(productId: number): Promise<ProductModel | undefined>;
  insert(product: Product): Promise<Product>;
}

export class ProductRepository implements IProductRepository {
  private connection: Connection;

  constructor() {
    this.connection = getConnection();
  }

  async getProductById(productId: number): Promise<ProductModel> {
    const product = await this.connection
      .getRepository(ProductModel)
      .createQueryBuilder("product")
      .where("product.id = :id", { id: productId })
      .getOne();

    return product;
  }

  async getAllProductsByPage(page: number): Promise<ProductModel[]> {
    const products = await this.connection
      .getRepository(ProductModel)
      .createQueryBuilder("product")
      .skip(page * 20)
      .take(20)
      .getMany();

    return products;
  }

  async insert(product: Product): Promise<Product> {
    const createdProduct = await this.connection
      .createQueryBuilder()
      .insert()
      .into(ProductModel)
      .values([
        {
          currency: product.getCurrency.getValue,
          description: product.getDescription,
          image: product.getImage,
          name: product.getName,
          pricePerUnit: product.getPricePerUnit,
        },
      ])
      .execute();

    const productId = createdProduct.identifiers[0].id;
    product.setId = productId;

    return product;
  }
}
