import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { TAvaiableCurrencies } from "../../domain/value-object/Currency";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  pricePerUnit: number;

  @Column({
    type: "enum",
    enum: ["PLN", "EUR"],
  })
  currency: TAvaiableCurrencies;

  @Column({
    nullable: true,
  })
  image: string;
}
