export enum INFRASTRUCTURE_ERROR {
  NOT_FOUND_ERROR = "NOT_FOUND_ERROR",
}

export class InfrastructureError extends Error {
  infrastructureError: INFRASTRUCTURE_ERROR;

  constructor(infrastructureError: INFRASTRUCTURE_ERROR, msg?: string) {
    super(msg);
    Object.setPrototypeOf(this, InfrastructureError.prototype);
    this.infrastructureError = infrastructureError;
  }
}
