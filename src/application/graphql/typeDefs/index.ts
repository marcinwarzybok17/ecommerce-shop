import { root } from "./root";
import { product } from "./product";

export const typeDefs = [root, product];
