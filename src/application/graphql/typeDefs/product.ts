import { gql } from "apollo-server";

export const product = gql`
  type Product {
    id: Int!
    name: String!
    description: String!
    pricePerUnit: Float!
    currency: Currency!
    image: String
  }

  enum Currency {
    PLN
    EUR
    USD
  }
`;
