import { gql } from "apollo-server";

export const root = gql`
  type Query {
    getProductById(productId: Int!): Product!
  }
`;
