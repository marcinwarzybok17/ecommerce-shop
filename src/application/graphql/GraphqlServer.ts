import { allowStaticMethods } from "../../utils/allowStaticMethods";
import { ApolloServer } from "apollo-server";
import { Server, ServerStaticMethods } from "../servers/Server";
import { root } from "./typeDefs/root";
import { product } from "./typeDefs/product";
import { Resolvers } from "../../generated/graphql";
import { ProductRepository } from "../../infrastructure/repository/ProductRepository";
import { ProductService } from "../../infrastructure/services/ProductService";
import { ProductController } from "./resolvers/Query/ProductController";

export type IGraphQlServer = Server<ApolloServer>;

@allowStaticMethods<ServerStaticMethods<ApolloServer>>()
export class GraphQlServer implements IGraphQlServer {
  private server: ApolloServer;
  port: number;

  constructor() {
    const typeDefs = [root, product];
    const productRepository = new ProductRepository();
    const productService = new ProductService(productRepository);
    const productController = new ProductController(productService);
    const resolvers: Resolvers = {
      Query: {
        ...productController.createResolvers(),
      },
    };
    this.server = new ApolloServer({
      typeDefs,
      resolvers,
      playground: true,
      introspection: true,
    });
    console.log("GraphQlServer started");
  }

  getServerInstance(): ApolloServer {
    return this.server;
  }

  async start(port: number): Promise<void> {
    await this.server.listen({
      port,
    });
  }

  async close(): Promise<void> {
    await this.server.stop();
  }

  static async runServer(port: number): Promise<Server<ApolloServer>> {
    const server = new GraphQlServer();
    await server.start(port);
    return server;
  }
}
