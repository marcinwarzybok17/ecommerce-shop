import { ApolloError } from "apollo-server";
import {
  DomainError,
  DOMAIN_ERROR,
  TDOMAIN_ERROR,
} from "../../../domain/common/DomainError";
import { InfrastructureError } from "../../../infrastructure/common/InfrastructureError";

export interface ErrorHandler {
  [key: string]: () => never;
}

export class GraphqlControllerErrorHandler {
  static handleError(err: Error, errorHandler) {
    if (err instanceof DomainError && errorHandler[err.domainError]) {
      errorHandler[err.domainError]();
    }

    if (
      err instanceof InfrastructureError &&
      errorHandler[err.infrastructureError]
    ) {
      errorHandler[err.infrastructureError]();
    }

    // Connect to some form of error logging system i.e. sentry
    throw new ApolloError("Internal Server Error", "INTERNAL_SERVER_ERROR");
  }
}
