import { ApolloError } from "apollo-server";
import { GRAPHQL_ERROR } from "./GraphqlError";

export class NotFoundError extends ApolloError {
  constructor(message: string) {
    super(message, GRAPHQL_ERROR.NOT_FOUND_ERROR);
    Object.defineProperty(this, "name", {
      value: GRAPHQL_ERROR.NOT_FOUND_ERROR,
    });
  }
}
