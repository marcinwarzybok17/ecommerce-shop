import { QueryResolvers } from "../../../generated/graphql";

export interface GraphqlController<T extends keyof QueryResolvers> {
  createResolvers(): Pick<QueryResolvers, T>;
}
