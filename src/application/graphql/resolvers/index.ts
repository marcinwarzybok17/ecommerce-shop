import { Resolvers } from "../../../generated/graphql";
import { ProductRepository } from "../../../infrastructure/repository/ProductRepository";
import { ProductService } from "../../../infrastructure/services/ProductService";
import { ProductController } from "./Query/ProductController";

const productRepository = new ProductRepository();
const productService = new ProductService(productRepository);
const productController = new ProductController(productService);

export const resolvers: Resolvers = {
  Query: {
    ...productController.createResolvers(),
  },
};
