import { ApolloError } from "apollo-server";
import { DOMAIN_ERROR } from "../../../../domain/common/DomainError";
import { ProductMapper } from "../../../../domain/mapper/ProductMap";
import { Product, QueryResolvers } from "../../../../generated/graphql";
import { INFRASTRUCTURE_ERROR } from "../../../../infrastructure/common/InfrastructureError";
import { IProductService } from "../../../../infrastructure/services/ProductService";
import {
  ErrorHandler,
  GraphqlControllerErrorHandler,
} from "../../errors/GraphqlControllerErrorHandler";
import { GRAPHQL_ERROR } from "../../errors/GraphqlError";
import { NotFoundError } from "../../errors/NotFoundError";
import { GraphqlController } from "../GraphqlController";

type ProductQueryResolvers = "getProductById";

export interface IProductController {
  getProductById(...args): Promise<Product>;
}

export class ProductController
  implements IProductController, GraphqlController<ProductQueryResolvers>
{
  productService: IProductService;

  constructor(productService: IProductService) {
    this.productService = productService;
  }

  createResolvers(): Pick<QueryResolvers<any, any>, "getProductById"> {
    return {
      getProductById: this.getProductById.bind(this),
    };
  }

  async getProductById(_parent, { productId }, _context): Promise<Product> {
    try {
      const product = await this.productService.getProductById(productId);
      return ProductMapper.fromDTOToGraphqlResult(product);
    } catch (err) {
      const errorHandler: ErrorHandler = {
        [DOMAIN_ERROR.ENTITY_NOT_FOUND_ERROR]: () => {
          throw new ApolloError(err.message, GRAPHQL_ERROR.NOT_FOUND_ERROR);
        },
        [INFRASTRUCTURE_ERROR.NOT_FOUND_ERROR]: () => {
          throw new ApolloError(err.message, GRAPHQL_ERROR.NOT_FOUND_ERROR);
        },
      };
      GraphqlControllerErrorHandler.handleError(err, errorHandler);
    }
  }
}
