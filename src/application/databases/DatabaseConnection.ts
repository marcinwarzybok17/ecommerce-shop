export interface DatabaseConnection {
  open(): Promise<void> | void;
  close(): Promise<void> | void;
}

export interface DatabaseConnectionStaticMethods {
  create(): Promise<DatabaseConnection>;
}
