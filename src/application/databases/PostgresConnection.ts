import {
  DatabaseConnection,
  DatabaseConnectionStaticMethods,
} from "./DatabaseConnection";
import { Connection, createConnection } from "typeorm";
import { allowStaticMethods } from "../../utils/allowStaticMethods";

export interface IPostgresConnection extends DatabaseConnection {
  open(): Promise<void>;
  close(): Promise<void>;
}

@allowStaticMethods<DatabaseConnectionStaticMethods>()
export class PostgresConnection implements IPostgresConnection {
  private connectin: Connection;

  static async create(): Promise<IPostgresConnection> {
    const postgresConnection = new PostgresConnection();
    await postgresConnection.open();
    return postgresConnection;
  }

  async open(): Promise<void> {
    this.connectin = await createConnection();
    console.log("Connected to Postgres Database");
  }
  async close(): Promise<void> {
    await this.connectin.close();
  }
}
