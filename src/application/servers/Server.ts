export interface Server<T> {
  start(port: number): Promise<void>;
  close(): Promise<void>;
  getServerInstance(): T;
}

export interface ServerStaticMethods<T> {
  runServer(port: number): Promise<Server<T>> | Server<T>;
}
