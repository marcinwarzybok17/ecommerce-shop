import { allowStaticMethods } from "../../utils/allowStaticMethods";
import { Product as ProductModel } from "../../infrastructure/model/Product";
import { Product } from "../entity/Product";
import { Mapper } from "../common/Mapper";
import { IProductDTO, ProductDTO } from "../dto/ProductDTO";
import { Currency, AvaiableCurrencies } from "../value-object/Currency";
import { Currency as GraphqlCurrency } from "../../generated/graphql";

@allowStaticMethods<Mapper<ProductModel, Product, IProductDTO>>()
export class ProductMapper {
  static fromPersistanceToDomain(persistanceObject: ProductModel) {
    return new Product(
      {
        currency: new Currency(persistanceObject.currency),
        description: persistanceObject.description,
        name: persistanceObject.name,
        pricePerUnit: persistanceObject.pricePerUnit,
        image: persistanceObject.image,
      },
      persistanceObject.id
    );
  }

  static fromPersistanceToDTO(persistanceObject: ProductModel) {
    return new ProductDTO(
      persistanceObject.id,
      persistanceObject.name,
      persistanceObject.description,
      persistanceObject.pricePerUnit,
      persistanceObject.currency,
      persistanceObject.image
    );
  }

  static fromDomainToDTO(domainObject: Product) {
    return new ProductDTO(
      domainObject.getId,
      domainObject.getName,
      domainObject.getDescription,
      domainObject.getPricePerUnit,
      domainObject.getCurrency.getValue,
      domainObject.getImage
    );
  }

  static fromDTOToGraphqlResult(dtoObject: ProductDTO) {
    const currencyMapper = {
      [AvaiableCurrencies.EUR]: GraphqlCurrency.Eur,
      [AvaiableCurrencies.PLN]: GraphqlCurrency.Pln,
    };

    return {
      id: dtoObject.id,
      currency: currencyMapper[dtoObject.currency],
      description: dtoObject.description,
      name: dtoObject.name,
      pricePerUnit: dtoObject.pricePerUnit,
      image: dtoObject.image,
    };
  }
}
