import { AvaiableCurrencies } from "../value-object/Currency";

export interface IProductDTO {
  id: number | string;
  name: string;
  description: string;
  pricePerUnit: number;
  currency: AvaiableCurrencies;
  image?: string;
}

export class ProductDTO implements IProductDTO {
  id: number;
  name: string;
  description: string;
  pricePerUnit: number;
  currency: AvaiableCurrencies;
  image?: string;

  constructor(
    id: number,
    name: string,
    description: string,
    pricePerUnit: number,
    currency: AvaiableCurrencies,
    image?: string
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.pricePerUnit = pricePerUnit;
    this.currency = currency;
    this.image = image;
  }

  get getId() {
    return this.id;
  }

  get getName() {
    return this.name;
  }

  get getDescription() {
    return this.description;
  }

  get getPricePerUnit() {
    return this.pricePerUnit;
  }

  get getCurrency() {
    return this.currency;
  }

  get getImage() {
    return this.image;
  }
}
