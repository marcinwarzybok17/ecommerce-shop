import { DomainError, DOMAIN_ERROR } from "../common/DomainError";
import { Currency } from "../value-object/Currency";
import { Entity } from "./../common/Entity";

export interface ProductProps {
  name: string;
  description: string;
  pricePerUnit: number;
  currency: Currency;
  image?: string;
}

export class Product extends Entity<number> {
  private props?: Partial<ProductProps>;

  constructor(props?: ProductProps, id?: number) {
    if (!props) {
      throw new DomainError(
        `Product is not valid`,
        DOMAIN_ERROR.ENTITY_NOT_FOUND_ERROR
      );
    }

    super(id);
    this.props = {};
    this.setName = props.name;
    this.setDescription = props.description;
    this.setCurrency = props.currency;
    this.setPricePerUnit = props.pricePerUnit;
    this.setImage = props.image;
  }

  get getId() {
    return this.id;
  }

  get getName() {
    return this.props.name;
  }

  get getDescription() {
    return this.props.description;
  }

  get getPricePerUnit() {
    return this.props.pricePerUnit;
  }

  get getCurrency() {
    return this.props.currency;
  }

  get getImage() {
    return this.props.image;
  }

  set setName(name: string) {
    this.props.name = name;
  }

  set setDescription(description: string) {
    this.props.description = description;
  }

  set setPricePerUnit(pricePerUnit: number) {
    this.props.pricePerUnit = pricePerUnit;
  }

  set setCurrency(currency: Currency) {
    this.props.currency = currency;
  }

  set setImage(image: string) {
    if (typeof image === "string") {
      this.props.image = image;
    }
  }
}
