import { DomainError, DOMAIN_ERROR } from "../common/DomainError";

export type TAvaiableCurrencies = AvaiableCurrencies;
export enum AvaiableCurrencies {
  "EUR" = "EUR",
  "PLN" = "PLN",
}

export class Currency {
  private readonly listOfAvaiableCurrencies = [
    AvaiableCurrencies.EUR,
    AvaiableCurrencies.PLN,
  ];

  constructor(private currency: AvaiableCurrencies) {
    this.setCurrency = currency;
  }

  get getValue() {
    return this.currency;
  }

  set setCurrency(currency: AvaiableCurrencies) {
    if (typeof currency === "undefined" || typeof currency !== "string") {
      throw new DomainError(
        `Currency is not defined`,
        DOMAIN_ERROR.VALIDATION_ERROR
      );
    }

    if (!this.isCurrencyAvaiable(currency)) {
      throw new DomainError(
        `Currency ${currency} is not avaiable`,
        DOMAIN_ERROR.VALIDATION_ERROR
      );
    }
  }

  isCurrencyAvaiable(currency: AvaiableCurrencies): boolean {
    return this.listOfAvaiableCurrencies.includes(currency);
  }
}
