export enum DOMAIN_ERROR {
  ENTITY_NOT_FOUND_ERROR = "ENTITY_NOT_FOUND_ERROR",
  VALIDATION_ERROR = "VALIDATION_ERROR",
}

export type TDOMAIN_ERROR = DOMAIN_ERROR;

export class DomainError extends Error {
  domainError: DOMAIN_ERROR;

  constructor(msg: string, domainError: DOMAIN_ERROR) {
    super(msg);
    Object.setPrototypeOf(this, DomainError.prototype);
    this.domainError = domainError;
  }
}
