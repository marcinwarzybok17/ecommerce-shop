export interface IEntity<IDType> {
  id?: IDType;
}

export class Entity<IDType> implements IEntity<IDType> {
  id?: IDType;

  constructor(id?: IDType) {
    this.id = id;
  }

  get getId() {
    return this.getId;
  }

  set setId(id: IDType) {
    this.id = id;
  }
}
