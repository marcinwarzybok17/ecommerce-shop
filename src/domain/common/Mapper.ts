export interface Mapper<Persistance, Domain, DTO> {
  fromPersistanceToDomain(persistanceObject: Persistance): Domain;
  fromPersistanceToDTO(persistanceObject: Persistance): DTO;
}
