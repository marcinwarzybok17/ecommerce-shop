import "dotenv/config";

export default {
  graphqlPort: Number(process.env.GRAPHQL_SERVER_PORT),
};
