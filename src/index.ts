import { MainApp } from "./MainApp";
import "reflect-metadata";

(async () => {
  const main = MainApp.getMainApp();
  await main.start();
})();
