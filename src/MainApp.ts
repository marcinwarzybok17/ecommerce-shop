import { ApolloServer } from "apollo-server";
import "reflect-metadata";
import { DatabaseConnection } from "./application/databases/DatabaseConnection";
import { PostgresConnection } from "./application/databases/PostgresConnection";
import { GraphQlServer } from "./application/graphql/GraphqlServer";
import { Server } from "./application/servers/Server";
import config from "./utils/config";

export class MainApp {
  private static instance: MainApp;
  postgresConnection: DatabaseConnection;
  graphqlServer: Server<ApolloServer>;

  private constructor() {}

  public static getMainApp(): MainApp {
    if (!MainApp.instance) {
      MainApp.instance = new MainApp();
    }

    return MainApp.instance;
  }

  async start() {
    this.postgresConnection = await PostgresConnection.create();
    this.graphqlServer = await GraphQlServer.runServer(config.graphqlPort);
  }

  async stop() {
    await this.postgresConnection.close();
    await this.graphqlServer.close();
  }
}
