export default {
  type: "postgres",
  host: "db",
  port: 5432,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: true,
  logging: true,
  entities: ["src/infrastructure/model/**/*.ts"],
  migrations: ["src/infrastructure/migration/**/*.ts"],
  subscribers: ["src/infrastructure/subscriber/**/*.ts"],
  cli: {
    entitiesDir: "src/infrastructure/model",
    migrationsDir: "src/infrastructure/migration",
    subscribersDir: "src/infrastructure/subscriber",
  },
};
