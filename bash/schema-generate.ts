import { formatSdl } from "format-graphql";
import { typeDefs } from "../src/application/graphql/typeDefs/index";
import { print } from "graphql";
import { join } from "path";
import { writeFileSync } from "fs";

const generatedSchema = typeDefs
  .map((documentNode) => {
    return print(documentNode);
  })
  .join(",");

writeFileSync(
  join(__dirname, "../schema.graphql"),
  formatSdl(generatedSchema),
  { encoding: "utf8", flag: "w" }
);
