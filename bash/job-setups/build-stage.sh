#!/bin/bash
apt-get update && apt-get install --yes
apt-get install --yes curl software-properties-common
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install --yes nodejs
node -v
npm -v
npm install -g yarn
yarn 
yarn schema:generate
